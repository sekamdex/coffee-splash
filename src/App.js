import Background from "./components/Background";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Copy from "./components/Copy";

import "./App.css";

const App = () => {
  return (
    <>
      <div className="absolute flex flex-col justify-between -inset-x-0 px-4 py-4 w-screen h-screen lg:px-6 lg:py-5 z-10">
        <Header className="" />
        <Copy className="" />
        <Footer className="" />
      </div>
      <Background className="fixed h-screen w-screen inset-0 z-0" />
    </>
  );
};

export default App;

const Header = ({ className = "" }) => {
  return (
    <header className={className}>
      <h1 className="font-bold text-lg">Coffee Shop Logo</h1>
    </header>
  );
};

export default Header;

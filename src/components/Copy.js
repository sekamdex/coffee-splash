const Copy = ({ className = "" }) => {
  return (
    <div className={className}>
      <div>
        <h2 className="font-serif text-6xl mb-8 text-red-900">
          The best coffee to start your day.
        </h2>
        <button className="bg-red-500 hover:bg-red-400 text-white font-medium pt-1 pb-1.5 px-4 rounded ">
          Find a shop now!
        </button>
      </div>
    </div>
  );
};

export default Copy;

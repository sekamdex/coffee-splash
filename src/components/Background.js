import { Canvas, useFrame, useLoader, useThree } from "@react-three/fiber";
import { DepthOfField, EffectComposer } from "@react-three/postprocessing";
import { Environment, useGLTF } from "@react-three/drei";
import { Suspense, useRef, useState } from "react";
import * as THREE from "three";

useGLTF.preload("/models/cup.glb");

const Cup = ({ z }) => {
  const ref = useRef();
  const { viewport, camera } = useThree();
  const { width, height } = viewport.getCurrentViewport(
    camera,
    new THREE.Vector3(0, 0, z)
  );

  const { nodes } = useGLTF("/models/cup.glb");
  const colorMap = useLoader(THREE.TextureLoader, "./models/cup-1024.png");
  colorMap.flipY = false;

  const [data] = useState({
    x: THREE.MathUtils.randFloatSpread(2),
    y: THREE.MathUtils.randFloatSpread(height),
    rX: Math.random() * Math.PI,
    rY: Math.random() * Math.PI,
    rZ: Math.random() * Math.PI,
  });

  useFrame(() => {
    if (!ref.current) return;

    ref.current.position.set(data.x * width, (data.y += 0.005), z);

    ref.current.rotation.set(
      (data.rX += 0.001),
      (data.rY += 0.004),
      (data.rZ += 0.005)
    );

    if (data.y > height) data.y = -height;
  });

  return (
    <group scale={1.5} ref={ref} dispose={null}>
      <mesh geometry={nodes.Cylinder003.geometry}>
        <meshBasicMaterial map={colorMap} />
      </mesh>
      <mesh geometry={nodes.Cylinder003_1.geometry}>
        <meshBasicMaterial map={colorMap} />
      </mesh>
      <mesh geometry={nodes.Cylinder003_2.geometry}>
        <meshBasicMaterial map={colorMap} />
      </mesh>
    </group>
  );
};

const Effects = ({ depth }) => {
  return (
    <EffectComposer>
      <DepthOfField
        target={[0, 0, depth / 2]}
        focalLength={0.75}
        bokehScale={2}
        height={700}
      />
    </EffectComposer>
  );
};

const Background = ({ count = 100, depth = 80, className = "" }) => {
  return (
    <div className={className}>
      <Canvas gl={{ alpha: false }} camera={{ near: 0.01, far: 110, fov: 30 }}>
        <color attach="background" args={["#6B4F4F"]} />
        <spotLight position={[10, 10, 10]} intensity={1} />
        <Suspense fallback={null}>
          {Array.from({ length: count }, (e, i) => (
            <Cup key={i} z={(-i / count) * depth - 10} />
          ))}
        </Suspense>
        <Effects depth={depth} />
      </Canvas>
    </div>
  );
};

export default Background;
